# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core import validators


class Picture(models.Model):
    """docstring for Picture"""
    s_order = models.IntegerField()
    picture = models.ImageField(
        upload_to='documents/%Y/%m/%d'
    )
    caption = models.CharField(max_length=128)


class Table(models.Model):
    """docstring for Table"""
    s_order = models.IntegerField()
    table = models.TextField()
    caption = models.CharField(max_length=128)


class List(models.Model):
    """docstring for Table"""
    LIST_TYPE_CHOICES = (
        ('ORDERED', 'Ordered'),
        ('UNORDERED', 'Unordered'),
    )
    s_order = models.IntegerField()
    list_type = models.CharField(max_length=12, choices=LIST_TYPE_CHOICES)
    list = models.TextField()


class Text(models.Model):
    """docstring for Text"""
    s_order = models.IntegerField()
    text = models.TextField()


class Subtitle(models.Model):
    """docstring for Subtitle"""
    subtitle = models.CharField(max_length=128)
    s_order = models.IntegerField()
    texts = models.ManyToManyField(Text)
    pictures = models.ManyToManyField(Picture)
    tables = models.ManyToManyField(Table)
    lists = models.ManyToManyField(List)


class Title(models.Model):
    """docstring for Title"""
    section_title = models.CharField(max_length=128)
    t_order = models.IntegerField()
    texts = models.ManyToManyField(Text)
    pictures = models.ManyToManyField(Picture)
    tables = models.ManyToManyField(Table)
    lists = models.ManyToManyField(List)


class Methodology(models.Model):
    """docstring for Methodology"""
    passive_recognition = models.ForeignKey(
        Subtitle,
        related_name='passive_recognition'
    )
    active_recognition = models.ForeignKey(
        Subtitle,
        related_name='active_recognition'
    )
    vulnerabilities_identificication = models.ForeignKey(
        Subtitle,
        related_name='vulnerabilities_identificication'
    )
    subtitles = models.ManyToManyField(Subtitle)
    pictures = models.ManyToManyField(Picture)
    tables = models.ManyToManyField(Table)
    lists = models.ManyToManyField(List)


class CompromisedSoftware(models.Model):
    """docstring for CompromisedSoftware"""
    COMPROMISED_SOFTWARE_TYPE_CHOICES = (
        ('SERVER', 'Server'),
        ('WEB_SOCKET', 'Websocket'),
        ('P2PI', 'P2P Interface'),
        ('TOP_CLIENT', 'Top Client'),
    )
    type = models.CharField(max_length=12, choices=COMPROMISED_SOFTWARE_TYPE_CHOICES)
    detail = models.CharField(max_length=254)


class Level(models.Model):
    """docstring for Level"""
    LEVEL_CHOICES = (
        ('HIGH', 'High'),
        ('MID', 'Mid'),
        ('LOW', 'Low'),
    )
    criticality = models.CharField(max_length=4, choices=LEVEL_CHOICES)
    impact = models.CharField(max_length=4, choices=LEVEL_CHOICES)
    effort = models.CharField(max_length=4, choices=LEVEL_CHOICES)
    score = models.DecimalField(
        max_digits=3,
        decimal_places=1,
        validators=[
            validators.MinValueValidator(1.0),
            validators.MaxValueValidator(10.0),
        ]
    )


class Vulnerability(models.Model):
    """docstring for Vulnerability"""
    code = models.CharField(max_length=32)
    vulnerability_title = models.CharField(max_length=128)
    detail = models.CharField(max_length=254)
    compromised_software = models.ForeignKey(CompromisedSoftware)
    evidences = models.TextField()
    recommendation = models.TextField()
    level = models.ForeignKey(Level)


class Vulnerabilities(models.Model):
    """docstring for Vulnerabilities"""
    s_order = models.IntegerField()
    section_title = models.CharField(max_length=128)
    pharaghraph_introduction = models.TextField()
    vulnerabilities_list = models.ManyToManyField(Vulnerability)


class Conclusion(models.Model):
    """docstring for Conclusion"""
    texts = models.ManyToManyField(Text)
    lists = models.ManyToManyField(List)


class Document(models.Model):
    """docstring for Document"""
    REPORT_TYPE_CHOICES = (
        ('INTERNAL', 'Internal'),
        ('EXTERNAL', 'External'),
    )
    document_title = models.CharField(max_length=128, default='Report')
    company_name = models.CharField(max_length=128, blank=True)
    report_type = models.CharField(max_length=10, choices=REPORT_TYPE_CHOICES, default='EXTERNAL')
    introduction = models.ForeignKey(Title, related_name='introduction')
    methodology = models.ForeignKey(Methodology)
    titles = models.ManyToManyField(Title)
    vulnerabilities = models.ForeignKey(Vulnerabilities)
    recommendations = models.ForeignKey(Title, related_name='recommendations')
    conclusion = models.ForeignKey(Conclusion)
