Proyecto desarrollado en Django con SQLite, permite generar reportes de seguridad informática de forma automática.

Para implantar el proyecto una vez clonado el repositorio se siguen los sigueintes comandos:

* Instalar pip:

```shell
$ curl -O http://python-distribute.org/distribute_setup.py
$ python distribute_setup.py
$ curl -O https://raw.github.com/pypa/pip/master/contrib/get-pip.py
$ python get-pip.py
```

* Instalar Virtualenv:

```shell
$ pip install virtualenv
```

* Ir al repositorio local:

```shell
$ cd /path/to/directory/
```

* Crear un ambiente virtual e instalar requerimientos:

```shell
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```    
